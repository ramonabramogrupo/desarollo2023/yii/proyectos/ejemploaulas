DROP DATABASE IF EXISTS aulas2023;
CREATE DATABASE aulas2023;
USE aulas2023;

CREATE TABLE cursos(
id int,
nombre varchar(400),
temario varchar(200),
PRIMARY KEY(id)
);

CREATE TABLE alumnos(
id int,
nombre varchar(300),
apellidos varchar(400),
email varchar(200),
telefono varchar(20),
foto varchar(200),
fecha timestamp DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(id)
);

CREATE TABLE contenido(
id int,
documento varchar(200),
foto varchar(200),
idCurso int,
PRIMARY KEY(id)
);

CREATE TABLE matricula(
id int AUTO_INCREMENT,
idAlumno int,
idCurso int,
fechaMatricula timestamp,
PRIMARY KEY(id),
UNIQUE KEY(idAlumno,idCurso,fechaMatricula)

);

ALTER TABLE matricula 
  ADD CONSTRAINT fkMatriculaAlumnos FOREIGN KEY (idAlumno) REFERENCES alumnos(id),
  ADD CONSTRAINT fkMatriculaCursos FOREIGN KEY (idCurso) REFERENCES cursos(id);


ALTER TABLE contenido
  ADD CONSTRAINT fkContenidoCursos FOREIGN KEY (idCurso) REFERENCES cursos(id);
